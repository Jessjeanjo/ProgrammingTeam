//
//  multiples3and5.cpp
//
//
//  Created by Jessica Joseph on 10/14/15.
//
//


#include <iostream>

using namespace std;


int sumPrime(int caseTesty){
    // Takes an int parameter and returns the sum of all multiples of 3 and 5 below that parameter
    
    int multiInt = 0;
    
    for (int i = 1; i < caseTesty; i++){
        if (i % 3 == 0 || i % 5 == 0) //Testing value to see if number is a multiple of 3 or 5
        multiInt += i;
    
    }
    return multiInt;
}

int main() {
    
    int testCases, testVal;
    
    cin >> testCases;
    
    for (int i = 0; i < testCases; i++){
        cin >> testVal;
        cout << sumPrime(testVal) << endl;
    }
    
    return 0;
}

/*
 Solution Explanation:
 
 This current solution is a naive solution which works for small testcases. 
 This solution has a time complexity of O(n^2), using Big-Oh notation.
 
 I created a separate function sumPrime which takes an int parameter and returns the sum of
 all of the multiples below that int. This function could have also been done recursively.
 
 Noted Solution Inefficencies:
 For larger test cases this function becomes very inefficent.

 If there are multiple testcases and the previous testcase is smaller than the current testcase 
 then I should be able to just build off of the previous. (Currently not implemented)
 
 Noted check: Would it be more efficient to solve the sumPrime recursively as opposed to iteratively?
 */