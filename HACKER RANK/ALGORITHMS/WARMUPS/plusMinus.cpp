//
//  plusMinus.cpp
//  
//
//  Created by Jessica Joseph on 10/8/15.
//
//

#include <iostream>

using namespace std;


int main() {
//You're given an array containing integer values. You need to print the fraction of count of positive numbers, negative numbers and zeroes to the total numbers. Print the value of the fractions correct to 3 decimal places.
    
    int arrVal, arrSize, zeroCnt=0, negativeCnt =0, positiveCnt = 0;
    cin >> arrSize;
    
    int myArr[arrSize];
    
    for (int i =0; i < arrSize; i++){
        cin >> arrVal;
        
        if (arrVal == 0)
            zeroCnt += 1;
        else if (arrVal < 0){
            negativeCnt += 1;
            
        } else
            positiveCnt += 1;
    }
    
    
    cout << double(positiveCnt)/double(arrSize) << endl << double(negativeCnt)/double(arrSize) << endl << double(zeroCnt)/double(arrSize);
    
    
    
    return 0;
    
    
    
    /*
     
     Sample Input
     
     6
     -4 3 -9 0 4 1
     
     Sample Output
     
     0.500
     0.333
     0.167
     
     */
}
