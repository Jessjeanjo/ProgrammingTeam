//
//  simpleArraySum.cpp
//  
//
//  Created by Jessica Joseph on 10/7/15.
//
//

#include <iostream>

using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    
    int arrSize, arrInput, arrSum=0;
    
    cin >> arrSize;
    
    for(int i = 0; i < arrSize; i++){
        cin >> arrInput;
        arrSum += arrInput;
    }
    
    cout << arrSum;
    return 0;
    
    /*
     
     Sample Test Input
     6
     1 2 3 4 10 11
     
     */
     
}

