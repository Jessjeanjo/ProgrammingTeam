//
//  aVeryBigSum.cpp
//  
//
//  Created by Jessica Joseph on 10/7/15.
//
//

#include <iostream>

using namespace std;

//Code is exactly like simpleArraySum, except for the change from int to long long int for the input and sum

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int arrSize;
    long long int arrInput, arrSum=0;
    
    cin >> arrSize;
    
    for(int i = 0; i < arrSize; i++){
        cin >> arrInput;
        arrSum += arrInput;
    }
    
    cout << arrSum;
    
    return 0;
    
    /*
     
     Sample Test Input
     5
     1000000001 1000000002 1000000003 1000000004 1000000005
     
     */
    
    
}
