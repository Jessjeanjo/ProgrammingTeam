//
//  diagonalDifference.cpp
//  
//
//  Created by Jessica Joseph on 10/7/15.
//
//

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int matriSize, leftD, rightD, leftSum=0, rightSum=0;
    
    cin >> matriSize;
    
    for (int i =0; i < matriSize; i++){
        
        for(int j=0; j < i; j++)
            cin >> leftD;
        
        for(int k=matriSize; k < i; k--)
            cin >> rightD;
        
        leftSum += leftD;
        rightSum += rightD;
    }
    
    cout << abs(leftSum - rightSum);
    return 0;
}
