//
//  staircase.cpp
//  
//
//  Created by Jessica Joseph on 10/8/15.
//
//

#include <iostream>

using namespace std;


int main() {
//Your teacher has given you the task to draw the structure of a staircase. Being an expert programmer, you decided to make a program for the same. You are given the height of the staircase. You need to print a staircase as shown in the example.
    
    int stairHeight;
    cin >> stairHeight;
    
    for (int i =1; i <= stairHeight; i++){
        
        cout << string((stairHeight - i), ' ') << string(i, '#') << endl;
    }
    
    return 0;
    
    /*
     
     Sample Input
     
     6
     
     
     Sample Output
     
     #
     ##
     ###
     ####
     #####
     ######
     
     */
}
