//
//  reverseLinkedList.cpp
//  
//
//  Created by Jessica Joseph on 10/9/15.
//
//

#include <iostream>
#include "LinkedList.h"


using namespace std;

int main(){
    
    //Problem One
//    int arr[7] = {5, 7, 9, 1, 6};
//    int arr2[3] = {6, 3, 2};
//    
    
    int startInt = 13;
    LList<int>* l1 = new LList<int>();
    
    for (int i; i < 3; i++){
        l1->push_back(startInt);
        startInt += 3;

    }
    
    l1->getHead()->listDisplay();
    
    LListNode<int> *firstPointy = l1->head->next;
    LListNode<int> *secondPointy = l1->head->next->next;
    
    
    l1->head->next = NULL; // head next is now pointing to null
    
    firstPointy->next = l1->head; // second element now points to head
    
    
    secondPointy->next = firstPointy; // third element now points to second element
    
    l1->head = secondPointy; // reassign the head to equal the last element
    
    l1->head->listDisplay();
    
}
