//
//  LinkedList.h
//  
//
//  Created by Jessica Joseph on 10/9/15.
//
//

#ifndef _LinkedList_h
#define _LinkedList_h


#endif


//
//  Linked Lists.cpp
//  Working
//
//  Created by Jessica Joseph on 8/7/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//

#include <iostream>

using namespace std;

template <class T>
class LList;

template <class T>
class LListNode{
public:
  
    LListNode<T>* next;
    T data;
    LListNode(T newdata = T(), LListNode<T>* newNext = NULL) :data(newdata), next(newNext){}
    friend class LList < T > ;
    void listDisplay() const;
    
    
};


template <class T>
class LList{
public:

    LListNode<T>* head;
    LListNode<T>* recursiveCopy(LListNode<T>* rhs);

    
    LList() :head(NULL){}
    ~LList(){ clear(); }
    LList(const LList<T>& rhs) :head(NULL){ *this = rhs; }
    bool isEmpty(){ return head == NULL; }
    void push_front(const T&);
    T pop_front();
    void clear(){ while (!isEmpty()) pop_front(); }
    int size()const;
    LListNode<T>* find(T toFind);
    void push_back(const T&);
    T pop_back();
    void push_after(T newElement, LListNode<T>* ptr);
    
    void splice(LListNode<T> *insertAf, LList<T>* newList);
    LListNode<T>* subList(LList<T>& bigList, LList<T>& checkList);
    LListNode<T>* getHead() const { return head; }
    
    
    LList<T>& operator=(const LList<T>& rhs);
    LListNode<T>* listSearch(LList<T>* sub);
    
};


template <class T>
LListNode<T>* LList<T>::recursiveCopy(LListNode<T>* rhs){
    if (rhs == NULL)
        return NULL;
    return new LListNode<T>(rhs->data, recursiveCopy(rhs->next));
}

template <class T>
LList<T>& LList<T>::operator=(const LList<T>& rhs){
    if (this == &rhs)
        return *this;
    clear();
    
    //Begin EVIL way of doing it
    /*
     LListNode<T>* temp = rhs.head;
     while (temp != NULL)
     push_back(temp->data);
     */
    //End EVIL way
    head = recursiveCopy(rhs.head);
    return *this;
}

template <class T>
void LList<T>::push_after(T newElement, LListNode<T>* ptr){
    ptr->next = new LListNode<T>(newElement, ptr->next);
}

template <class T>
T LList<T>::pop_back(){
    if (isEmpty())//pop empty????
        return T();
    else if (head->next == NULL)
        return pop_front();
    LListNode<T>* temp = head;
    while (temp->next->next != NULL) //stops on second to last node
        temp = temp->next;
    T retval = temp->next->data;
    LListNode<T>* toDelete = temp->next;
    temp->next = NULL;
    delete toDelete;
    return retval;
}

template <class T>
void LList<T>::push_back(const T& newdata){
    if (isEmpty())
        push_front(newdata);
    else{
        LListNode<T>* temp=head;
        while (temp->next != NULL)
            temp = temp->next;
        push_after(newdata, temp);
    }
}

template <class T>
LListNode<T>* LList<T>::find(T toFind){
    LListNode<T>* temp = head;
    for (; temp != NULL&& temp->data != toFind; temp = temp->next);
    return temp;
}

template <class T>
int LList<T>::size()const{
    int count = 0;
    for (LListNode<T>* temp = head; temp != NULL; temp = temp->next)
        count++;
    return count;
    
}

template <class T>
T LList<T>::pop_front(){
    if (isEmpty()) // pop on an empty list????
        return T();
    T retval = head->data;
    LListNode<T>* temp = head;
    head = head->next;
    delete temp;
    return retval;
}

template <class T>
void LList<T>::push_front(const T& newdata){
    head = new LListNode<T>(newdata, head);
}


/////////////HOMEWORK, P1
template <class T>
void LList<T>::splice(LListNode<T> * insertAf, LList<T>* newList){
    
    LListNode<T> *currList = head, *temp = find(insertAf->data);
    
    temp->next = newList->head;
    
    while (currList->next != NULL)
        currList = currList->next;
    
    find(currList->data)->next = temp;
    
}

template<class T>
void LListNode<T>::listDisplay() const
{
    cout << data << ' ';
    LListNode<T>* p = next;
    while (p != nullptr) {
        cout << p->data << ' ';
        p = p->next;
    }
    cout << endl;
}

/////////////HOMEWORK, P2
template <class T>
LListNode<T>* LList<T>::subList(LList<T>& bigList, LList<T>& checkList){
    if (find(checkList->head->data) == NULL)
        return NULL;
    
    LListNode<T>* currList = find(checkList->head->data), *checkLH = checkList->head, * subHead = checkList->head;
    
    while (checkLH->next != NULL && currList->next != NULL)
    {
        
        if (currList->next->data == checkLH->next->data)
        {
            currList = currList->next;
            checkLH = checkLH->next;
        }
        else
        {
            subHead = currList->next;
            currList = currList->next;
            checkLH = checkList->head;
        }
    }
    
    
    if (checkLH->next != NULL)
        return NULL;
    
    else if (currList->next == NULL && checkLH->next == NULL)
        return subHead;
    
    else if (checkLH->next == NULL)
        return subHead;
    
    else
        return NULL;
    
}

